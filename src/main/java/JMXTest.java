/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import javax.net.ssl.*;
import javax.rmi.ssl.SslRMIClientSocketFactory;
/**
 *
 * @author pablo
 */
public class JMXTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        
        String host;
        String port;
        String user;
        String pass;
        
        if (args != null && args.length > 0) {
            host = args[0];
        } else {
            host = "bms.choicebuildinginspections.com.au";
        }
        
        if (args != null && args.length > 1) {
            port = args[1];
        } else {
            port = "8686";
        }
        
        if (args != null && args.length > 2) {
            user = args[2];
        } else {
            user = "admin";
        }
        
        if (args != null && args.length > 3) {
            pass = args[3];
        } else {
            pass = "yama108";
        }
        
        /* */ 
        System.setProperty("com.sun.net.ssl.checkRevocation", "false");
        
        TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                    System.out.println("Trusting client " + Arrays.toString(certs));
                }
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                    System.out.println("Trusting server " + Arrays.toString(certs));
                }
            }
        };        
        
        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        sc.getSocketFactory();
        SSLContext.setDefault(sc);
        
        Logger.getLogger("javax.management.remote.rmi").setLevel(Level.ALL);
        String url = "service:jmx:rmi://" + host + ":" + port + "/jndi/rmi://" + host + ":" + port + "/jmxrmi";
        System.out.println("Connecting to " + url + " user=" + user + " pass=" + pass);
        //String url = "service:jmx:rmi:///jndi/rmi://skynet.linkssurveying.com.au:8886/jmxrmi";
        String[] creds = {user, pass};
        HashMap env = new HashMap();
        env.put(JMXConnector.CREDENTIALS, creds);
        //SslRMIClientSocketFactory rmisf = ;
        env.put("com.sun.jndi.rmi.factory.socket", new SslRMIClientSocketFactory());
        JMXConnector connector = JMXConnectorFactory.connect(new JMXServiceURL(url), env);
        System.out.println("Connected OK with " + connector);
        
        //rmi://notes.care:8886/jmxrmi
        
    }
    
}
